﻿using ConsoleAppdb2.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace ConsoleAppdb2
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello! Its a homework prog by Mikhail Dmitriev");
            StartDeleteAllAndCreate();
            MainMenu();
            var s=Console.ReadLine();
            while (s != "0")
            {
                switch (s)
                {
                    case "1":
                        MenuItemPrintAll();
                        break;
                    case "2":
                         MenuItemInsertLogic();
                        break;
                    case "0":
                        return;
                    default:
                        break;
                }               

                MainMenu();
                s = Console.ReadLine();               
            }
        }

        /// <summary>
        /// Меню для консольной программы
        /// </summary>
        static void MainMenu()
        {
            Console.WriteLine("To select menu item you should print a number and press ENTER ");
            Console.WriteLine("(1) - print all tables in DB; (2) Insert into table (0) Exit program");
        }

        /// <summary>
        /// Пункт меню - вывод информации содержащейся в бд
        /// </summary>
        static void MenuItemPrintAll()
        {
             using (ApplicationContext db = new ApplicationContext())
            {
               
                var users = db.Customers.ToList();
                var accounts=db.CustomersAccount.ToList();
                var operations=db.Operations.ToList();
                Console.WriteLine("List Of db object:");
                foreach (Customer u in users)
                {
                    Console.WriteLine($"{u.Id}.{u.Name} : Mail{u.Email} : Phone{u.Phone} ");
                    foreach(CustomerAccount c in u.CustomerAccounts)
                    {
                        Console.WriteLine($"  {c.Id} AccountNumber : {c.AccountNumber}");
                        foreach(Operation o in c.Operations)
                        {
                            Console.WriteLine($"    {o.Id} Operation : {o.OperSum}$");
                        }
                        Console.WriteLine("--------------------------");
                    }
                }

                
            }
        }

       /// <summary>
       /// Пункт меню вставки данных
       /// </summary>
        static void MenuItemInsert()
        {
            Console.WriteLine("Choose table to insert , print number and press enter");
            Console.WriteLine("(1) - customer; (2) account (3) operation (0) back to main menu");
            
        }


        /// <summary>
        /// Логика работы вставки данных
        /// </summary>
        static void MenuItemInsertLogic()
        {
            MenuItemInsert();
            var s=Console.ReadLine();
            while (s != "0")
            {
                switch (s)
                {
                    case "1":
                        InsertNewCustomerLogic();
                        break;
                    case "2":
                        InserNewAccLogic();
                        break;
                    case "3":
                        InserNewOperationLogic();
                        break;
                    case "0":
                        return;
                    default:
                        break;
                }               

                MenuItemInsert();
                s = Console.ReadLine();               
            }
        }

        /// <summary>
        /// Пункт меню вставки данных для таблицы пользователей
        /// </summary>
        static void MenuItemInsertCustomer()
        {
            Console.WriteLine("You are inserting new customer now");
            Console.WriteLine("(1) - start to enter customer; (0) back to insert menu");

        }            

        /// <summary>
        /// Обработка пункта меню вставки данных для таблицы пользователей
        /// </summary>
        static void InsertNewCustomerLogic()
        {
            MenuItemInsertCustomer();
            var s=Console.ReadLine();
            while (s != "0")
            {
                switch (s)
                {
                    case "1":
                        InsertNewCustomer();
                        break;
                    case "0":
                        return;
                    default:
                        break;
                }               

                MenuItemInsertCustomer();
                s = Console.ReadLine();               
            }
        }

        static void InsertNewCustomer()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                Console.WriteLine("Please Enter Customer Data:");
                Console.WriteLine("Name:");
                string? name=Console.ReadLine();
                Console.WriteLine("Mail:");
                string? mail=Console.ReadLine();
                Console.WriteLine("Phone:");
                string? phone=Console.ReadLine();

                Customer customer=new Customer{ Name=name, Email=mail,Phone=phone};
                db.Customers.Add(customer);
                db.SaveChanges();
             
            }
        }

        
        /// <summary>
        /// Пункт меню вставки данных для таблицы счетов
        /// </summary>
        static void MenuItemInsertAcc()
        {
            Console.WriteLine("You are inserting new accaunt now");
            Console.WriteLine("(1) - stаrt to enter accaunt; (0) back to insert menu");

        }            

        /// <summary>
        /// Обработка пункта меню вставки данных для таблицы счетов
        /// </summary>
        static void InserNewAccLogic()
        {
            MenuItemInsertAcc();
            var s=Console.ReadLine();
            while (s != "0")
            {
                switch (s)
                {
                    case "1":
                        InsertNewAcc();
                        break;
                    case "0":
                        return;
                    default:
                        break;
                }               

                MenuItemInsertAcc();
                s = Console.ReadLine();               
            }
        }

        /// <summary>
        /// Добавляем новый счет
        /// </summary>
        static void InsertNewAcc()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var customers=db.Customers.ToList();

                Console.WriteLine("Now in db exist current customers:");
                foreach(var customer in customers)
                {
                    Console.WriteLine($"ID - {customer.Id}; Name -{customer.Name}; Phone - {customer.Phone}");
                }

                Console.WriteLine("Please Choose Customer ID:");
                Console.WriteLine("Id:");
                int customerId=1;
                int.TryParse(Console.ReadLine(), out customerId);
                Console.WriteLine("Enter New Accaunt Number:");
                string? accaunt=Console.ReadLine();
                
                CustomerAccount ca=new CustomerAccount{AccountNumber=accaunt, CustomerId=customerId};
                
                db.CustomersAccount.Add(ca);
                db.SaveChanges();
             
            }
        }

        /// <summary>
        /// Пункт меню вставки данных для таблицы операций
        /// </summary>
        static void MenuItemInsertOperation()
        {
            Console.WriteLine("You are inserting new operation now");
            Console.WriteLine("(1) - stаrt to enter operation; (0) back to insert menu");

        }            

        /// <summary>
        /// Обработка пункта меню вставки данных для таблицы операций
        /// </summary>
        static void InserNewOperationLogic()
        {
            MenuItemInsertOperation();
            var s=Console.ReadLine();
            while (s != "0")
            {
                switch (s)
                {
                    case "1":
                        InsertNewOper();
                        break;
                    case "0":
                        return;
                    default:
                        break;
                }               

                MenuItemInsertOperation();
                s = Console.ReadLine();               
            }
        }

        /// <summary>
        /// Добавляем новую операцию
        /// </summary>
        static void InsertNewOper()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                var acc=db.CustomersAccount.ToList();
                var cust=db.Customers.ToList();

                Console.WriteLine("Now in db exist current accountts:");
                foreach(var account in acc)
                {
                    Console.WriteLine($"ID - {account.Id}; Number -{account.AccountNumber}; Name of Customer - {cust[account.CustomerId-1].Name}");
                }

                Console.WriteLine("Please Choose Account ID:");
                Console.WriteLine("Id:");
                int accountId=1;
                int.TryParse(Console.ReadLine(), out accountId);
                Console.WriteLine("Enter amount of operation:");
                int operSum=1;
                int.TryParse(Console.ReadLine(), out operSum);
                
                Operation op=new Operation{OperSum=operSum, CustomerAccountId=accountId};
                
                db.Operations.Add(op);
                db.SaveChanges();
             
            }
        }


        /// <summary>
        /// Стартовая процедура - здесь происходит пересоздание БД 
        /// </summary>
        static void StartDeleteAllAndCreate ()
        {
            using (ApplicationContext db = new ApplicationContext(true,true))
            {
                Console.WriteLine("Database has been inited");
 
                // получаем объекты из бд и выводим на консоль
                var users = db.Customers.ToList();
                var accounts=db.CustomersAccount.ToList();
                var operations=db.Operations.ToList();
                Console.WriteLine("List of DB object:");
                foreach (Customer u in users)
                {
                    Console.WriteLine($"{u.Id}.{u.Name} : Mail{u.Email} : Phone{u.Phone} ");
                    foreach(CustomerAccount c in u.CustomerAccounts)
                    {
                        Console.WriteLine($"  {c.Id} AccountNumber : {c.AccountNumber}");
                        foreach(Operation o in c.Operations)
                        {
                            Console.WriteLine($"    {o.Id} Operation : {o.OperSum}$");
                        }
                        Console.WriteLine("--------------------------");
                    }
                }                
            }
        }

        

    }


}
