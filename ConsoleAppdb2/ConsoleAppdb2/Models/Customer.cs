﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppdb2.Models
{
 
    [Index("Phone")]
    internal class Customer
    {
        public int Id { get; set; }

        [Required][MaxLength(50)]
        public string Name { get; set; }
        public string Email{get;set;}
        public string Phone{get;set;}

        public List<CustomerAccount> CustomerAccounts{ get; set; } = new List<CustomerAccount>();
    }
}
