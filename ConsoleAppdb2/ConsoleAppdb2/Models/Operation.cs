﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppdb2.Models
{
    internal class Operation
    {
        public int Id { get; set; }
        public int OperSum { get; set; }
        
        public int CustomerAccountId{get;set;}
        public CustomerAccount CustomerAccount{get;set;}
    }
}
