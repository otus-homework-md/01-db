﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppdb2.Models
{
    internal class CustomerAccount
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public List<Operation> Operations { get; set; }=new List<Operation>();
        
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        
    }
}
