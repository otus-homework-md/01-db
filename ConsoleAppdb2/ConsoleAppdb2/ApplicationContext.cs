﻿using ConsoleAppdb2.Models;
using ConsoleAppdb2.Properties;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppdb2
{
    internal class ApplicationContext:DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerAccount> CustomersAccount { get; set; }
        public DbSet<Operation> Operations { get; set; }

        /// <summary>
        /// Выводим или нет лог в консоль при создании БД
        /// </summary>
        public bool EnableLogging{get; set; }   =true;
         

        /// <summary>
        /// Конструктор контекста
        /// </summary>
        /// <param name="isFirstStart">При первом вызове - при запуске программы обнуляем БД и пересоздаем</param>
        /// <param name="isLogOn">Вывод лога в консоль при работе с БД - включаем при первом запуске и создании БД</param>
        public ApplicationContext(bool isFirstStart=false, bool isLogOn=false)
        {
            EnableLogging=isLogOn;

            if(isFirstStart)                  
                Database.EnsureDeleted();

            Database.EnsureCreated();
        }
 
        /// <summary>
        /// Подключаемся к БД
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(Resources.ConnectionString);

            if (EnableLogging)
                optionsBuilder.LogTo(System.Console.WriteLine);

        }

        /// <summary>
        /// Создание БД и наполнение первичными данными при необходимости
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasIndex(u => new { u.Email });
            modelBuilder.Entity<Customer>().HasIndex(u => u.Phone).HasDatabaseName("PhoneIndex");

            Customer[] customers =new Customer[] 
            {
                new Customer {Id=1,  Name="Tom", Email="1@mail.ru", Phone="+1234568"},
                new Customer {Id=2,  Name="Alice", Email="2@mail.ru", Phone="+1434568"},
                new Customer {Id=3,  Name="Sam", Email="3@mail.ru", Phone="+1334568"},
                new Customer {Id=4,  Name="Ben", Email="4@mail.ru", Phone="+1634568"},
                new Customer {Id=5,  Name="Nik", Email="5@mail.ru", Phone="+1734568"}
            };

            CustomerAccount[] CustomersAccounts=new CustomerAccount[] 
            {
                new CustomerAccount {Id=1,  AccountNumber="1234567890",CustomerId=1},
                new CustomerAccount {Id=2,  AccountNumber="0894256215",CustomerId=2},
                new CustomerAccount {Id=3,  AccountNumber="1245846852",CustomerId=3},
                new CustomerAccount {Id=4,  AccountNumber="1526347856",CustomerId=4},
                new CustomerAccount {Id=5,  AccountNumber="1523557874",CustomerId=5}
            };

            Operation[] operations =new Operation[]{
                new Operation{Id=1,OperSum=1000, CustomerAccountId=1 },
                new Operation{Id=2,OperSum=1000, CustomerAccountId=2},
                new Operation{Id=3,OperSum=1000, CustomerAccountId=3},
                new Operation{Id=4,OperSum=1000, CustomerAccountId=4},
                new Operation{Id=5,OperSum=1000, CustomerAccountId=5}
                };


            modelBuilder.Entity<Customer>().HasData(customers);

            modelBuilder.Entity<CustomerAccount>().HasData(CustomersAccounts);

            modelBuilder.Entity<Operation>().HasData(operations);
    
        }

    }
}
